<?php

namespace Zb\SalesOrderGridDb\Model\ResourceModel\Provider;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Sales\Model\ResourceModel\Provider\NotSyncedDataProviderInterface;

/**
 * Retrieve ID's of not synced entities by columns:
 * The result would list all ID's from the main table with missing attribute/s in the grid table
  */
class NotSyncedOrderListProvider implements NotSyncedDataProviderInterface
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var AdapterInterface
     */
    private $connection;

    /**
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->connection = $resourceConnection->getConnection('sales');
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @inheritdoc
     */
    public function getIds($mainTableName, $gridTableName)
    {
        $mainTableName = $this->resourceConnection->getTableName($mainTableName);
        $gridTableName = $this->resourceConnection->getTableName($gridTableName);

        // where condition
        $condition = '('.$gridTableName.'.coupon_code IS NULL AND '.$mainTableName.'.coupon_code IS NOT NULL';
        $condition .= ' OR '.$gridTableName.'.coupon_code <>'.$mainTableName.'.coupon_code)';
        $condition .= ' OR ('.$gridTableName.'.discount_amount IS NULL AND '.$mainTableName.'.discount_amount IS NOT NULL';
        $condition .= ' OR '.$gridTableName.'.discount_amount <>'.$mainTableName.'.discount_amount)';

        $select = $this->connection->select()
            ->from($mainTableName, [$mainTableName . '.entity_id'])
            ->joinInner(
                [$gridTableName => $gridTableName],
                sprintf(
                    '%s.entity_id = %s.entity_id',
                    $mainTableName,
                    $gridTableName
                ),
                []
            )
            ->where($condition);
        ;

        return $this->connection->fetchAll($select, [], \Zend_Db::FETCH_COLUMN);
    }
}
