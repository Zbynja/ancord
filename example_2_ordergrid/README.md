# Admin OrderGrid - New Columns


## TASK

Add 'coupon_code' and 'discount_amount' values/columns to admin order grid.


## 4 STRATEGIES
NOTE** These data are already stored in the sales_order table


### STRATEGY #1 - source DB table data duplication (Winning solution)

— duplicate the data as new columns in the sales_order_grid

CONS: data redundancy, overhead with crud operations (refresh/purge on order cancellation, failed payments, 3rd party order editing …) and data syncing on 2 places (sales_order vs. sales_order_grid), also a need for existing data sync, data removal with module uninstall …

PROS:

- gives the power to choose between sync/async (instant/scheduled) grid data update
- already a part of the core - many part are configuration only needed
- certain entities are already joined to grid via DI in core - e.g. Order, Order Address, Order Payment

NOTE**
Certain entities are already joined to grid via DI - e.g. Orders, Order Address, Payment

    vendor/magento/module-sales/etc/di.xml:488

we only need to add them via DI and enable scheduled grid updates and reindexing:

implementation

- install script to add a new field/s into sales_order_grid table
    $connection->addColumn(
        $setup->getTable('sales_order_grid'),
    ...

A/ for instant update

- observer to fetch the data on place-order (e.g. checkout_submit_all_after event) using the Orders Grid interface for instant data sync
    Magento\Sales\Model\ResourceModel\GridInterface::refresh()
- the observer should take into account possibility to schedule on update and skip updating if the option is set in the admin

B/ for update on schedule
For scheduled data sync (by cron) we would implement a custom NotSyncedDataProvider (NotSyncedDataProviderInterface) and add it to composite providers (Virtual Type)


FLOW:

    // asyncInsert() called by CRON and observers
    // see di.xml and crontab.xml
    
    // vendor/magento/module-sales/Model/GridAsyncInsert.php:55
    public function asyncInsert()
    {
        if ($this->globalConfig->getValue('dev/grid/async_indexing')) {
            $this->entityGrid->refreshBySchedule();
        }
    }
    
    // vendor/magento/module-sales/Model/ResourceModel/GridInterface.php:34
    refreshBySchedule();
    // -- implementation vendor/magento/module-sales/Model/ResourceModel/Grid.php:116
    $notSyncedIds = $this->notSyncedDataProvider->getIds($this->mainTableName, $this->gridTableName);
    // vendor/magento/module-sales/Model/ResourceModel/Provider/NotSyncedDataProvider.php:39
    public function getIds($mainTableName, $gridTableName)
    




### STRATEGY #2 - Data Fetch using a Plugin

— inject the new data somewhere during the collection data preparation 

CONS: 

- the plugin gets called on a general class = all grid requests (needs to be filtered by name)
- if another plugin uses exactly the same code, it would override - this can be a trap and needs to be handled somehow, e.g. we need to define a unique alias for the leftJoin table
- the system already provides collection join via DI

PROS: plugins are chain-able and preferable way of extending the system

where to interrupt?

Options:
option A/ 

    public function getReport($requestName)

— vendor/magento/framework/View/Element/UiComponent/DataProvider/CollectionFactory.php

- implement an after Plugin
- check for the $requestName - should be ‘sales_order_grid_data_source’ for the Orders Grid
    if ('sales_order_grid_data_source' == $requestName) ...
- then JOIN the orders table with the given attributes
    $collection->getSelect()->joinLeft(
        ["orders_table_1" => $joinTable],
        'main_table.entity_id = orders_table_1.entity_id',
        $this->joiningAttributes
    );

NOTE** !!
Make a unique table alias so that each module has its own, otherwise the next plugin in row with the same code will override the left join. That is why we use

        ["orders_table_1" => $joinTable],

links: 
[MageNest - cutomers grid]
[BeLVG tutorial]
[by SiarheyUchukhlebau - add region_id column to the orders grid]


option B/ 

    public function search(SearchCriteriaInterface $searchCriteria)

— vendor/magento/framework/View/Element/UiComponent/DataProvider/Reporting.php

- implement an after Plugin
- check for the collection’s main table name  - should be ‘sales_order_grid’
    if ($collection->getMainTable() === $collection->getConnection()->getTableName('sales_order_grid')) ...
- then JOIN the orders table with the given attributes (see above)

links: 
[by BeLVG - github]



### STRATEGY #3 - Custom Data Provider Collection - Argument Replacement (bad practice)

— override the sales_order_grid_data_source with a custom class extend
— add a leftJoin into the protected _renderFiltersBefore()

CONS: not a best practice - last module wins

- module A would extend the collection
    - https://snipboard.io/CMyghi.jpg
- but module B with the same logic would take over the collection overriding and would leave module A inactive
    - https://snipboard.io/rb40Lu.jpg

It is still a class replacement (rewrite) with the last-come-to-win strategy, even though more specific - in this case only for the UiComponent\DataProvider\CollectionFactory scope - defining the “sales_order_grid_data_source”

PROS: very specific - only applies for the “sales_order_grid_data_source” collection

— pass the custom class as argument into the DataProvider Collection Factory di.xml

        <type name="Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory">
            <arguments>
                <argument name="collections" xsi:type="array">
                    <item name="sales_order_grid_data_source" xsi:type="string">Zb\OrdersGrid\Model\ResourceModel\Order\Grid\Collection</item>
                </argument>
            </arguments>
        </type>

— override the protected function _renderFiltersBefore() - add the JOIN

    protected function _renderFiltersBefore()
    {
        $joinTable = $this->getTable('sales_order');
        $this->getSelect()->joinLeft($joinTable, 'main_table.entity_id = sales_order.entity_id', ['tax_amount', 'discount_amount', 'coupon_code']);
        parent::_renderFiltersBefore();
    }

links:
[by meetanshi]
[by magenable]



### STRATEGY #4 - Data Modifiers (bad practice)

DevDocs:

    Using modifiers is optional and might be necessary when static declaration in XML configuration files is not suitable for the tasks. For example, in cases when 
    additional data should be loaded from database. 

Has a huge performance impact if loading new entities, such as e.g. order (from repository)

- it is basically a for-each iteration