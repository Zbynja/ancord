<?php
declare(strict_types=1);

namespace Zb\SalesOrderGrid\Plugin\UiComponent\DataProvider;

use Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory;
use Magento\Framework\Data\Collection;

/**
 * Class CollectionFactoryPlugin
 * @package Zb\SalesOrderGrid\Plugin\UiComponent\DataProvider
 */
class CollectionFactoryPlugin
{
    const DATA_SOURCE_NAME = 'sales_order_grid_data_source';
    const JOIN_TABLE = 'sales_order';

    /**
     * @var array
     */
    private $joiningAttributes = [
        'discount_amount',
        'coupon_code'
    ];

    /**
     * @param CollectionFactory $subject
     * @param Collection $collection
     * @param string $requestName
     * @return Collection
     */
    public function afterGetReport(CollectionFactory $subject, Collection $collection, string $requestName): Collection
    {
        if (static::DATA_SOURCE_NAME == $requestName)
        {
            try {
                $joinTable = $collection->getConnection()->getTableName(static::JOIN_TABLE);
                $collection->getSelect()->joinLeft(
                    ["orders" => $joinTable],
                    'main_table.entity_id = orders.entity_id',
                    $this->joiningAttributes
                );
            } catch (\Zend_Db_Select_Exception $selectException) {
                // Silently fail / or log somewhere if needed
            }
        }

        return $collection;
    }
}
