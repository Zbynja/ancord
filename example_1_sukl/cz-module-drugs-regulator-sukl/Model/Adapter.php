<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Model;

use Drmax\DrugsRegulatorApi\Api\AdapterInterface;
use DrmaxCz\DrugsRegulatorSukl\Model\Adapter\Downloader;
use DrmaxCz\DrugsRegulatorSukl\Model\Adapter\Extractor;
use DrmaxCz\DrugsRegulatorSukl\Model\Adapter\Parser;

/**
 * Class Adapter
 * @package DrmaxCz\DrugsRegulatorSukl\Model
 */
class Adapter implements AdapterInterface
{
    /**
     * @var Downloader
     */
    private $downloader;

    /**
     * @var Extractor
     */
    private $extractor;

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @param Downloader $downloader
     * @param Extractor $extractor
     * @param Parser $parser
     */
    public function __construct(
        Downloader $downloader,
        Extractor $extractor,
        Parser $parser
    ) {
        $this->downloader = $downloader;
        $this->extractor = $extractor;
        $this->parser = $parser;
    }

    /**
     * @inheritDoc
     */
    public function getData(): array
    {
        $localFilePath = $this->downloader->downloadSuklDataFile();
        if ($localFilePath) {
            $csvData = $this->extractor->extractDataFromArchive($localFilePath);
            if ($csvData) {

                return $this->parser->getDataFromCsv($csvData);
            }
        }

        return [];
    }
}
