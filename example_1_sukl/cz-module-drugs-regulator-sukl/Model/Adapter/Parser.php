<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Model\Adapter;

use Drmax\DrugsRegulatorApi\Api\AttributesInterface as ApiAttributes;
use Drmax\DrugsRegulatorApi\Api\TimerInterface as Timer;
use DrmaxCz\DrugsRegulatorSukl\Config\Attributes as LocalAttributes;
use DrmaxCz\DrugsRegulatorSukl\Config\Config as ModuleConfig;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

class Parser
{
    /**
     * @var ModuleConfig
     */
    private $config;

    /**
     * @var Timer
     */
    private $timer;

    /**
     * @var ApiAttributes
     */
    private $apiAttributes;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Parser constructor.
     * @param ModuleConfig $config
     * @param ApiAttributes $attributes
     * @param Timer $timer
     * @param LoggerInterface $logger
     */
    public function __construct(
        ModuleConfig $config,
        ApiAttributes $apiAttributes,
        Timer $timer,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->apiAttributes = $apiAttributes;
        $this->timer = $timer;
        $this->logger = $logger;
    }


    /**
     * @param string $csvData
     * @return array|bool
     */
    public function getDataFromCsv(string $csvData): array
    {
        try {
            $this->timer->setStart();
            $returnData = [];
            $head = true;
            $attributesToUpdate = $this->apiAttributes->getAttributesToUpdate();

            $this->logger->debug('Start parsing CSV data');
            $csv = str_getcsv($csvData, "\n");
            foreach ($csv as $row) {
                $row = str_getcsv($row, ';');
                if ($head) {
                    $this->validateHeader($row);
                    $head = false;
                } else {
                    $suklProductData = [];
                    foreach ($attributesToUpdate as $attributeCode) {
                        if (ApiAttributes::DRMAX_DRUGS_REGULATOR_CODE === $attributeCode){
                            continue;
                        }
                        if (LocalAttributes::DRMAX_DRUGS_DRUG_CONTROL_NAME === $attributeCode){
                            $suklProductData[$attributeCode] = iconv(
                                'Windows-1250',
                                'UTF-8',
                                $row[$this->config->getSuklNameColumnId()] . ' ' . $row[$this->config->getSuklAdditionColumnId()]
                            );
                            continue;
                        }
                        $suklProductData[$attributeCode] = 'not defined';
                    }
                    $returnData[(int)$row[$this->config->getSuklCodeColumnId()]] = $suklProductData;
                }
            }
            $this->logger->debug('End parsing CSV data', ['duration' => $this->timer->getDuration()]);

            return $returnData;

        } catch (\Exception $e) {
            $this->logger->error('Cannot get data from csv ' . $e->getMessage(), ['exception' => $e]);
        }

        return false;
    }


    /**
     * @param array $row
     * @throws LocalizedException
     */
    public function validateHeader(array $row): void
    {
        $csvIndex = $this->config->getCsvIndex();
        foreach ($csvIndex as $suklParameter => $columnData) {
            if ($row[$columnData['column_id']] !== $columnData['column_name']) {
                throw new LocalizedException(__(
                    'Head for %1 should be %2 not %3',
                    $suklParameter,
                    $columnData['column_name'],
                    $row[$columnData['column_id']]
                ));
            }
        }
    }
}
