<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Model\Adapter;

use Drmax\DrugsRegulatorApi\Api\ConfigInterface as ApiConfig;
use Drmax\DrugsRegulatorApi\Api\TimerInterface as Timer;
use DrmaxCz\DrugsRegulatorSukl\Config\Config as ModuleConfig;
use Magento\Framework\Filesystem\Io\File;
use Psr\Log\LoggerInterface;

class Downloader
{
    /**
     * @var ModuleConfig
     */
    private $config;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var File
     */
    private $file;

    /**
     * @var Timer
     */
    private $timer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ModuleConfig $config
     * @param ApiConfig $apiConfig
     * @param File $file
     * @param Timer $timer
     * @param LoggerInterface $logger
     */
    public function __construct(
        ModuleConfig $config,
        ApiConfig $apiConfig,
        File $file,
        Timer $timer,
        LoggerInterface $logger
    ) {
        $this->apiConfig = $apiConfig;
        $this->config = $config;
        $this->file = $file;
        $this->timer = $timer;
        $this->logger = $logger;
    }

    /**
     * @return null|string
     */
    public function downloadSuklDataFile(): ?string
    {
        $this->timer->setStart();
        try {
            $sourceArchiveFile = $this->config->getSourceArchiveFile();
            $pathInfo = $this->file->getPathInfo($sourceArchiveFile);
            $fileName = $pathInfo['basename'];
            $localDirPath = $this->apiConfig->getReportFileDirPath();
            $localFilePath = $localDirPath . $fileName;
            $this->logger->debug('Start downloading SUKL file: ' . $sourceArchiveFile);

            /* Check if file path exist, create if not */
            $this->file->checkAndCreateFolder($localDirPath);
            $result = $this->file->read($sourceArchiveFile, $localFilePath);
            if ($result) {
                $this->logger->debug(
                    'End downloading SUKL file: ' . $sourceArchiveFile,
                    ['duration' => $this->timer->getDuration()]
                );

                return $localFilePath;
            }
            $this->logger->debug('The file was not downloaded successfully: ' . $sourceArchiveFile);

        } catch (\Exception $e) {
            $this->logger->error(
                'The file did not download. Exception thrown: ' . $e->getMessage(),
                ['exception' => $e]
            );
        }

        return null;
    }
}
