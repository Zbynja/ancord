<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Model\Adapter;

use Drmax\DrugsRegulatorApi\Api\TimerInterface as Timer;
use DrmaxCz\DrugsRegulatorSukl\Config\Config as ModuleConfig;
use Psr\Log\LoggerInterface;

class Extractor
{
    /**
     * @var ModuleConfig
     */
    private $config;

    /**
     * @var Timer
     */
    private $timer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ModuleConfig $config
     * @param LoggerInterface $logger
     * @param Timer $timer
     */
    public function __construct(
        ModuleConfig $config,
        Timer $timer,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->timer = $timer;
        $this->logger = $logger;
    }

    /**
     * @param string $archiveFile
     * @return string|bool
     */
    public function extractDataFromArchive(string $archiveFile): string
    {
        $this->timer->setStart();
        $this->logger->debug('Start extracting data from archive');
        $zip = new \ZipArchive();
        $zip->open($archiveFile);
        $fileData = $zip->getFromName($this->config->getSourceFile());
        $zip->close();
        ($fileData === false)
            ? $this->logger->error('Cannot extract file from archive')
            : $this->logger->debug('End extract data from archive', ['duration' => $this->timer->getDuration()]);

        return $fileData;
    }
}
