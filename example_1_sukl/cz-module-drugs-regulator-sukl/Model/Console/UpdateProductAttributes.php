<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Model\Console;

use Drmax\DrugsRegulatorApi\Api\UpdaterInterface as Updater;
use Drmax\DrugsRegulatorApi\Api\ConfigInterface as ApiConfig;
use DrmaxCz\DrugsRegulatorSukl\Model\Adapter;

/**
 * Class UpdateProductAttributes
 */
class UpdateProductAttributes
{
    /**
     * @var ApiConfig
     */
    private $config;

    /**
     * @var Adapter
     */
    private $adapter;

    /**
     * @var Updater
     */
    private $updater;

    /**
     * UpdateProductAttributes constructor.
     * @param ApiConfig $config
     * @param Adapter $adapter
     * @param Updater $updater
     */
    public function __construct(
        ApiConfig $config,
        Adapter $adapter,
        Updater $updater
    ) {
        $this->config = $config;
        $this->adapter = $adapter;
        $this->updater = $updater;
    }

    /**
     * Run the console command
     */
    public function execute(): void
    {
        if ($this->config->isEnabled()) {
            $suklData = $this->adapter->getData();
            $this->updater->updateProductAttributes($suklData);
        }
    }
}
