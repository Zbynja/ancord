<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'DrmaxCz_DrugsRegulatorSukl',
    __DIR__
);
