<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Setup\Patch\Data;

use DrmaxCz\DrugsRegulatorSukl\Config\Attributes;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Zend_Validate_Exception;

/**
 * Class AddAttributeDrugControlName
 * @package DrmaxCz\DrugsRegulatorSukl\Setup\Patch\Data
 */
class AddAttributeDrugControlName implements DataPatchInterface
{
    /**
     * Settings for Create/Update 'SUKL název' Product Attribute
     */
    private const REGULATOR_PRODUCT_NAME_SETTINGS = [
        'type' => 'varchar',
        'input' => 'text',
        'label' => 'Drugs Regulator (SUKL) Product Name',
        'used_in_product_listing' => true,
        'user_defined' => true,
        'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
        'visible_on_front' => true,
        'is_used_in_grid' => true,
        'is_visible_in_grid' => true,
        'is_filterable_in_grid' => true,
    ];

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $existingAttribute = $eavSetup->getAttribute(Product::ENTITY, Attributes::DRMAX_DRUGS_DRUG_CONTROL_NAME);
        (isset($existingAttribute['attribute_id']))
            ? $eavSetup->updateAttribute(Product::ENTITY, Attributes::DRMAX_DRUGS_DRUG_CONTROL_NAME, static::REGULATOR_PRODUCT_NAME_SETTINGS)
            : $eavSetup->addAttribute(Product::ENTITY, Attributes::DRMAX_DRUGS_DRUG_CONTROL_NAME, static::REGULATOR_PRODUCT_NAME_SETTINGS)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases(): array
    {
        return [];
    }
}
