<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Config;

/**
 * Local Attributes
 */
class Attributes
{
    public const DRMAX_DRUGS_DRUG_CONTROL_NAME = 'drmax_drugs_drug_control_name';
}
