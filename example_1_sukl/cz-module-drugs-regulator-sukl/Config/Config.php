<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Config
 */
class Config
{
    private const XML_PATH_SOURCE_ARCHIVE_FILE = 'integrations/drugs_regulator/source_archive_file';
    private const XML_PATH_SOURCE_FILE = 'integrations/drugs_regulator/source_file';

    /**
     * Indexes in csv file
     */
    private const XML_PATH_SUKL_CODE_COLUMN_ID = 'integrations/drugs_regulator/indexes_in_csv_file/sukl_code_column_id';
    private const XML_PATH_SUKL_CODE_COLUMN_NAME = 'integrations/drugs_regulator/indexes_in_csv_file/sukl_code_column_name';
    private const XML_PATH_SUKL_NAME_COLUMN_ID = 'integrations/drugs_regulator/indexes_in_csv_file/sukl_name_column_id';
    private const XML_PATH_SUKL_NAME_COLUMN_NAME = 'integrations/drugs_regulator/indexes_in_csv_file/sukl_name_column_name';
    private const XML_PATH_SUKL_ADDITION_COLUMN_ID = 'integrations/drugs_regulator/indexes_in_csv_file/sukl_addition_column_id';
    private const XML_PATH_SUKL_ADDITION_COLUMN_NAME = 'integrations/drugs_regulator/indexes_in_csv_file/sukl_addition_column_name';

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param DirectoryList $directoryList
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        DirectoryList $directoryList,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->directoryList = $directoryList;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function getSourceArchiveFile()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_SOURCE_ARCHIVE_FILE);
    }

    /**
     * @return string
     */
    public function getSourceFile()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_SOURCE_FILE);
    }

    public function getCsvIndex()
    {
        return [
            'sukl_code' => [
                'column_id' => $this->getSuklCodeColumnId(),
                'column_name' => $this->getSuklCodeColumnName()
            ],
            'sukl_name' => [
                'column_id' => $this->getSuklNameColumnId(),
                'column_name' => $this->getSuklNameColumnName()
            ],
            'sukl_addition' => [
                'column_id' => $this->getSuklAdditionColumnId(),
                'column_name' => $this->getSuklAdditionColumnName()
            ]
        ];
    }

    /**
     * @return int
     */
    public function getSuklCodeColumnId()
    {
        return (int)$this->scopeConfig->getValue(self::XML_PATH_SUKL_CODE_COLUMN_ID);
    }

    /**
     * @return string
     */
    public function getSuklCodeColumnName()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_SUKL_CODE_COLUMN_NAME);
    }

    /**
     * @return int
     */
    public function getSuklNameColumnId()
    {
        return (int)$this->scopeConfig->getValue(self::XML_PATH_SUKL_NAME_COLUMN_ID);
    }

    /**
     * @return string
     */
    public function getSuklNameColumnName()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_SUKL_NAME_COLUMN_NAME);
    }

    /**
     * @return int
     */
    public function getSuklAdditionColumnId()
    {
        return (int)$this->scopeConfig->getValue(self::XML_PATH_SUKL_ADDITION_COLUMN_ID);
    }

    /**
     * @return string
     */
    public function getSuklAdditionColumnName()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_SUKL_ADDITION_COLUMN_NAME);
    }
}
