<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Console\Command;

use DrmaxCz\DrugsRegulatorSukl\Model\Console\UpdateProductAttributes as CommandModel;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Console\Cli;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateProductAttributes
 */
class UpdateProductAttributes extends Command
{
    public const COMMAND_NAME = 'drmax:sukl:update';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var State
     */
    private $state;

    /**
     * @var CommandModel
     */
    private $commandModel;

    /**
     * @param LoggerInterface $logger
     * @param State $state
     * @param CommandModel $commandModel
     * @param string|null $name
     */
    public function __construct(
        LoggerInterface $logger,
        State $state,
        CommandModel $commandModel,
        string $name = null
    ) {
        $this->logger = $logger;
        $this->state = $state;
        $this->commandModel = $commandModel;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Update Drugs Regulator (SUKL) Products Attributes');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        return $this->state->emulateAreaCode(Area::AREA_ADMINHTML, function () use (&$input, &$output) {
            $this->process($input, $output);
        });
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function process(InputInterface $input, OutputInterface $output): int
    {
        $startTime = microtime(true);
        try {
            $output->writeln('Start');
            $this->commandModel->execute();
            $duration = round(microtime(true) - $startTime, 0) . 's';
            $output->writeln('Finish. Duration: ' . $duration);
            $this->logger->info(self::COMMAND_NAME . ' finish', ['duration' => $duration]);

            return Cli::RETURN_SUCCESS;

        } catch (\Exception $e) {
            $duration = round(microtime(true) - $startTime, 0) . 's';
            $output->writeln('Fail. Duration: ' . $duration);
            $output->writeln('Exception: ' . $e->getMessage());
            $this->logger->error(self::COMMAND_NAME . ' finish with errors', ['exception' => $e]);
        }

        return Cli::RETURN_FAILURE;
    }
}
