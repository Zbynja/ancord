<?php
declare(strict_types=1);

namespace DrmaxCz\DrugsRegulatorSukl\Ui\DataProvider\Product\Form\Modifier;

use DrmaxCz\DrugsRegulatorSukl\Config\Attributes;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;

/**
 * Class SuklNameDisable
 * @package Drmax\PIM\Ui\DataProvider\Product\Form\Modifier
 */
class SuklNameDisable extends AbstractModifier
{
    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * Attributes constructor.
     * @param ArrayManager $arrayManager
     */
    public function __construct(ArrayManager $arrayManager)
    {
        $this->arrayManager = $arrayManager;
    }

    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data): array
    {
        return $data;
    }

    /**
     * Changes the input field of EAV
     *
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta): array
    {
        $path = $this->arrayManager->findPath(Attributes::DRMAX_DRUGS_DRUG_CONTROL_NAME, $meta, null, 'children');
        if ($path) {
            // Is optional - only in some Attribute sets. Do not throw exception if not in path
            $meta = $this->arrayManager->set(
                "{$path}/arguments/data/config/disabled",
                $meta,
                true
            );
        }

        return $meta;
    }
}