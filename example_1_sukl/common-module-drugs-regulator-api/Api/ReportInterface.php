<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulatorApi\Api;

/**
 * Class ReportInterface
 * @package Drmax\DrugsRegulatorApi\Api
 * @api
 */
interface ReportInterface
{
    /**
     * @param array $rowData
     * @return void
     */
    public function addErrorRow(array $rowData): void;

    /**
     * @return void
     */
    public function saveToCsv(): void;

    /**
     * @return null|string
     */
    public function getFilePath(): ?string;

    /**
     * @return null|string
     */
    public function getFileName(): ?string;

    /**
     * @return bool
     */
    public function isFileExist(): bool;
}
