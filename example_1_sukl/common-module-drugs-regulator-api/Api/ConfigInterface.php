<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulatorApi\Api;

/**
 * Class ConfigInterface
 * @package Drmax\DrugsRegulatorApi\Api
 * @api
 */
interface ConfigInterface
{
    /**
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @return string
     */
    public function getReportFileName(): string;

    /**
     * @return string
     */
    public function getReportFileDirPath(): string;

    /**
     * @return array
     */
    public function getAttributeSetIds(): array;
}
