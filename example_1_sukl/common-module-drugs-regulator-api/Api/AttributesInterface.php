<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulatorApi\Api;

/**
 * Interface AttributesInterface
 * @package Drmax\DrugsRegulatorApi\Api
 * @api
 */
interface AttributesInterface
{
    /**
     * Product attributes
     */
    public const DRMAX_DRUGS_REGULATOR_CODE = 'drmax_drugs_drug_control_code';
    public const DRMAX_DRUGS_REGULATOR_CODE_LABEL = 'Drugs regulator ID';

    /**
     * AttributesInterface constructor.
     * @param array $attributesToUpdate
     */
    public function __construct(array $attributesToUpdate = []);

    /**
     * @return array
     */
    public function getAttributesToUpdate(): array;
}
