<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulatorApi\Api;

/**
 * Interface UpdaterInterface
 * @package Drmax\DrugsRegulatorApi\Api
 * @api
 */
interface UpdaterInterface
{
    /**
     * @param array $regulatorProductsData
     * @return void
     */
    public function updateProductAttributes(array $regulatorProductsData): void;
}
