<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulatorApi\Api;

/**
 * Interface AdapterInterface
 * @package Drmax\DrugsRegulatorApi\Api
 * @api
 */
interface AdapterInterface
{
    /**
     * @return array
     */
    public function getData(): array;
}
