<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulatorApi\Api;

/**
 * Interface TimerInterface
 * @package Drmax\DrugsRegulatorApi\Api
 * @api
 */
interface TimerInterface
{
    /**
     * @return float|null
     */
    public function getStart(): ?float;

    /**
     * @return void
     */
    public function setStart(): void;

    /**
     * @return string
     */
    public function getDuration(): string;
}
