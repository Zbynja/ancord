# show case #1 - refactoring SUKL


## TASK
Take the existing SUKL (National Drug Control Agency for Czechia) implementation and refactor it in align with the current design pattern needs of the client

- existing module - Sukl/Drmax_Sukl

- new modules package:
 - common parts
 	-- common-module-drugs-regulator-api
 	-- common-module-drugs_regulator
 	-- common-module-drugs-regulator-admin-ui
 - local part
	 -- cz-module-drugs-regulator-sukl


## design patterns / principles used

1/ common vs local
split into common/local parts


2/ module/package splitting 
- Api, impl. AdminUi, local extend/adapter

    - keep it non-split when it makes sense - e.g. local admin UI is just 2 files (system.xml + i18) - would create 4 more files for a separate module - does not make much sense 

3/ strict types 

    <?php
    declare(strict_types=1);

4/ Config interface pattern 
(see CFL)

5/ AttributesInterface pattern 
- attributes to be exposed to outer world should be part of the API

6/ Pool pattern in API interface
CFL page: Pool pattern in API module - constructor in AttributesInterface (sufficient for this case, otherwise would deserve its own class - not possible to extend the constructor with other params)

7/ Dynamic attributes 
- more can be added just via di.xml, not hard-coded in constants/code

    -- app/code/DrmaxCz/DrugsRegulatorSukl/etc/di.xml:4
    <type name="Drmax\DrugsRegulatorApi\Api\AttributesInterface">
        <arguments>
            <argument name="attributesToUpdate" xsi:type="array">
                <item name="drugsRegulatorProductName" xsi:type="const">DrmaxCz\DrugsRegulatorSukl\Config\Attributes::DRMAX_DRUGS_DRUG_CONTROL_NAME</item>
            </argument>
- the local attribute is also created/installed within the local module

8/ No Helpers
- get rid of Helpers - not needed any more (M1 mindset)

9/ SOLID principles 
- separate classes for Download, Extract, Parse ..

10/ Composition over Inheritance 
- evolution of the GetDuration function to Interface
    - we could have it as Abstract class but then the inheritance would create a trap for us - using it elsewere or from another module