<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulatorAdminUi\Block\Adminhtml\System\Config\Form;

use Drmax\DrugsRegulatorApi\Api\ReportInterface as ReportModel;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\UrlInterface;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Report
 */
class Report extends Field
{
    /**
     * @var ReportModel
     */
    private $report;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @param Context $context
     * @param ReportModel $report
     * @param UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        ReportModel $report,
        UrlInterface $urlBuilder,
        $data = []
    ) {
        $this->report = $report;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function _getElementHtml(AbstractElement $element): string
    {
        if ($this->report->isFileExist()) {
            $url = $this->urlBuilder->getUrl('drmax_drugsregulator/download/report');
            $element->setData(['href' => $url, 'value' => $this->report->getFileName()]);
        } else {
            $element->setData(['value' => 'No Report is currently available']);
        }

        return parent::_getElementHtml($element);
    }
}
