<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Drmax_DrugsRegulatorAdminUi',
    __DIR__
);
