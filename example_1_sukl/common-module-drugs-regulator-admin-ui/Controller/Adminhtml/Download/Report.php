<?php

namespace Drmax\DrugsRegulatorAdminUi\Controller\Adminhtml\Download;

use Drmax\DrugsRegulatorApi\Api\ReportInterface as ReportModel;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Controller\Adminhtml\System;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;

/**
 * Class Report
 */
class Report extends System
{
    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var ReportModel
     */
    private $report;

    /**
     * @param Context $context
     * @param FileFactory $fileFactory
     * @param ReportModel $report
     */
    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        ReportModel $report
    ) {
        $this->fileFactory = $fileFactory;
        $this->report = $report;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        $filePath = $this->report->getFilePath();
        $fileName = $this->report->getFileName();
        try {
            return $this->fileFactory->create(
                $fileName,
                [
                    'type' => 'filename',
                    'value' => $filePath
                ]
            );
        } catch (\Exception $e) {
            throw new NotFoundException(__($e->getMessage()));
        }
    }
}
