<?php

namespace Drmax\Sukl\Setup\Patch\Data;

use Drmax\Sukl\Helper\Attributes;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class SuklProductAttributes
 */
class SuklProductAttributes implements DataPatchInterface
{
    private const SUKL_ID = Attributes::DRMAX_DRUGS_DRUG_CONTROL_CODE;
    private const SUKL_NAME = Attributes::DRMAX_DRUGS_DRUG_CONTROL_NAME;

    /**
     * Setting for Create/Update 'SUKL kód' Product Attribute
     */
    private const SUKL_ID_SETTINGS = [
        'type' => 'varchar',
        'input' => 'text',
        'label' => 'SUKL kód',
        'used_in_product_listing' => true,
        'user_defined' => true,
        'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
        'visible_on_front' => true,
        'is_used_in_grid' => true,
        'is_visible_in_grid' => true,
        'is_filterable_in_grid' => true,
    ];

    /**
     * Setting for Create/Update 'SUKL název' Product Attribute
     */
    private const SUKL_NAME_SETTINGS = [
        'type' => 'varchar',
        'input' => 'text',
        'label' => 'SUKL název',
        'used_in_product_listing' => true,
        'user_defined' => true,
        'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
        'visible_on_front' => true,
        'is_used_in_grid' => true,
        'is_visible_in_grid' => true,
        'is_filterable_in_grid' => true,
    ];

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     * @throws LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $suklIdAttribute = $eavSetup->getAttribute(Product::ENTITY, self::SUKL_ID);
        (isset($suklIdAttribute['attribute_id']))
            ? $eavSetup->updateAttribute(Product::ENTITY, self::SUKL_ID, self::SUKL_ID_SETTINGS)
            : $eavSetup->addAttribute(Product::ENTITY, self::SUKL_ID, self::SUKL_ID_SETTINGS);

        $suklNameAttribute = $eavSetup->getAttribute(Product::ENTITY, self::SUKL_NAME);
        (isset($suklNameAttribute['attribute_id']))
            ? $eavSetup->updateAttribute(Product::ENTITY, self::SUKL_NAME, self::SUKL_NAME_SETTINGS)
            : $eavSetup->addAttribute(Product::ENTITY, self::SUKL_NAME, self::SUKL_NAME_SETTINGS);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
