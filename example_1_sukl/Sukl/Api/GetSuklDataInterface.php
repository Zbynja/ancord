<?php

namespace Drmax\Sukl\Api;

/**
 * Interface GetSuklDataInterface
 */
interface GetSuklDataInterface
{
    /**
     * @return array|bool
     */
    public function execute();
}
