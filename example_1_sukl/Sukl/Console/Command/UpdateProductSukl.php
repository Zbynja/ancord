<?php

namespace Drmax\Sukl\Console\Command;

use Drmax\Sukl\Model\UpdateProductSukl as UpdateProductSuklModel;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Console\Cli;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateProductSukl
 */
class UpdateProductSukl extends Command
{
    public const COMMAND_NAME = 'drmax:sukl:update';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var State
     */
    private $state;

    /**
     * @var UpdateProductSuklModel
     */
    private $updateProductSukl;

    /**
     * @param LoggerInterface $logger
     * @param State $state
     * @param UpdateProductSuklModel $updateProductSukl
     * @param string|null $name
     */
    public function __construct(
        LoggerInterface $logger,
        State $state,
        UpdateProductSuklModel $updateProductSukl,
        string $name = null
    ) {
        $this->logger = $logger;
        $this->state = $state;
        $this->updateProductSukl = $updateProductSukl;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Update Products SUKL Attribute');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        return $this->state->emulateAreaCode(Area::AREA_ADMINHTML, function () use (&$input, &$output) {
            $this->process($input, $output);
        });
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function process(InputInterface $input, OutputInterface $output)
    {
        $startTime = microtime(true);
        try {
            $output->writeln('Start');
            $this->updateProductSukl->execute();
            $duration = round(microtime(true) - $startTime, 0) . 's';
            $output->writeln('Finish. Duration: ' . $duration);
            $this->logger->info(self::COMMAND_NAME . ' finish', ['duration' => $duration]);
            return Cli::RETURN_SUCCESS;
        } catch (\Exception $e) {
            $duration = round(microtime(true) - $startTime, 0) . 's';
            $output->writeln('Fail. Duration: ' . $duration);
            $output->writeln('Exception: ' . $e->getMessage());
            $this->logger->error(self::COMMAND_NAME . ' finish with errors', ['exception' => $e]);
            return Cli::RETURN_FAILURE;
        }
    }
}
