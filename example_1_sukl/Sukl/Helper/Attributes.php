<?php

namespace Drmax\Sukl\Helper;

use Drmax\EavCreator\Helper\Attributes as EavAttributes;

/**
 * Class Attributes
 */
class Attributes
{
    public const DRMAX_DRUGS_DRUG_CONTROL_CODE = EavAttributes::DRMAX_DRUGS_DRUG_CONTROL_CODE;
    public const DRMAX_DRUGS_DRUG_CONTROL_NAME = EavAttributes::DRMAX_DRUGS_DRUG_CONTROL_NAME;
}
