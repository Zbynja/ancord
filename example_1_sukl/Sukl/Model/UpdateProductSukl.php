<?php

namespace Drmax\Sukl\Model;

use Drmax\Sukl\Api\GetSuklDataInterface;
use Drmax\Sukl\Helper\Attributes;
use Drmax\Sukl\Helper\Config as ConfigHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Exception\FileSystemException;
use Psr\Log\LoggerInterface;

/**
 * Class UpdateProductSukl
 */
class UpdateProductSukl
{
    private const SUKL_ID = Attributes::DRMAX_DRUGS_DRUG_CONTROL_CODE;
    private const SUKL_NAME = Attributes::DRMAX_DRUGS_DRUG_CONTROL_NAME;

    /**
     * @var ConfigHelper
     */
    private $config;

    /**
     * @var GetSuklDataInterface
     */
    private $getSuklData;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Report
     */
    private $report;

    /**
     * @param ConfigHelper $config
     * @param GetSuklDataInterface $getSuklData
     * @param LoggerInterface $logger
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param Report $report
     */
    public function __construct(
        ConfigHelper $config,
        GetSuklDataInterface $getSuklData,
        LoggerInterface $logger,
        ProductCollectionFactory $productCollectionFactory,
        ProductRepositoryInterface $productRepository,
        Report $report
    ) {
        $this->config = $config;
        $this->getSuklData = $getSuklData;
        $this->logger = $logger;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productRepository = $productRepository;
        $this->report = $report;
    }

    /**
     * @throws FileSystemException
     */
    public function execute()
    {
        if ($this->config->isEnabled()) {
            $suklData = $this->getSuklData->execute();
            $this->updateRelatedProducts($suklData);
        }
    }

    /**
     * @param $suklData
     * @return bool
     * @throws FileSystemException
     */
    private function updateRelatedProducts($suklData)
    {
        $attributeSetIds = $this->config->getAttributeSetIds();

        /** @var ProductCollection $productCollection */
        $productCollection = $this->productCollectionFactory->create();
        $products = $productCollection
            ->addAttributeToSelect([self::SUKL_ID, self::SUKL_NAME])
            ->addAttributeToFilter('attribute_set_id', ['in' => $attributeSetIds])
            ->load();

        $namesNotChanged = 0;
        $namesChanged = 0;
        $namesNotFound = 0;
        $errors = 0;
        $startTime = microtime(true);
        $this->logger->debug('Start updating SUKL name for ' . $products->count() . ' products.');

        /** @var Product $product */
        foreach ($products as $product) {
            $productId = $product->getId();
            $suklId = (int)$product->getData(self::SUKL_ID);
            $suklName = $product->getData(self::SUKL_NAME);
            if (array_key_exists($suklId, $suklData)) {
                $suklNameUpdate = $suklData[$suklId];
                if ($suklName === $suklNameUpdate) {
                    $namesNotChanged++;
                } else {
                    $product->setCustomAttribute(self::SUKL_NAME, $suklNameUpdate);
                    try {
                        $this->productRepository->save($product);
                        $namesChanged++;
                    } catch (\Exception $e) {
                        $this->logger->error('Can\'t save Product with Id %1.', ['exception' => $e]);
                        $this->report->addRowToReport($productId, $product->getSku(), $suklId, $e->getMessage());
                        $errors++;
                    }
                }
            } else {
                $this->logger->error(__(
                    'Product %1 has SUKL number %2 but not found in SUKL database.',
                    $productId,
                    $suklId
                ));
                $this->report->addRowToReport($productId, $product->getSku(), $suklId, 'Not found in SUKL database');
                $namesNotFound++;
            }
        }
        $this->logger->info(__('DONE: spent total %1', $this->getDuration($startTime)));
        $this->logger->info(__(
            'DONE: Items changed: %1. Items not changed: %2. Items not found: %3. Errors: %4',
            $namesChanged,
            $namesNotChanged,
            $namesNotFound,
            $errors
        ));
        $this->report->saveReportToCsv();
        return true;
    }

    /**
     * @param float $startTime
     * @return string
     */
    private function getDuration($startTime)
    {
        return round(microtime(true) - $startTime, 2) . 's';
    }
}
