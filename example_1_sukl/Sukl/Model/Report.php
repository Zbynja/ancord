<?php

namespace Drmax\Sukl\Model;

use Drmax\Sukl\Helper\Config as ConfigHelper;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\File\Csv as FileCsv;
use Magento\Framework\Filesystem\Io\File;
use Psr\Log\LoggerInterface;

/**
 * Class UpdateProductSukl
 */
class Report
{
    /**
     * @var ConfigHelper
     */
    private $config;

    /**
     * @var File
     */
    private $file;

    /**
     * @var FileCsv
     */
    private $fileCsv;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $errorReport = [];

    /**
     * @param ConfigHelper $config
     * @param File $file
     * @param FileCsv $fileCsv
     * @param LoggerInterface $logger
     */
    public function __construct(
        ConfigHelper $config,
        File $file,
        FileCsv $fileCsv,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->file = $file;
        $this->fileCsv = $fileCsv;
        $this->logger = $logger;
    }

    /**
     * @param string $productId
     * @param string $productSku
     * @param int $suklId
     * @param string $message
     * @return void
     */
    public function addRowToReport($productId, $productSku, $suklId, $message)
    {
        $this->errorReport[] = [
            'product_id' => $productId,
            'product_sku' => $productSku,
            'sukl_id' => $suklId,
            'message' => $message
        ];
    }

    /**
     * @throws FileSystemException
     */
    public function saveReportToCsv()
    {
        $header = ['Product Id', 'Product SKU', 'SUKL Code', 'Message'];
        array_unshift($this->errorReport, $header);
        $localFilePath = $this->getReportFilePath();
        if ($localFilePath) {
            $this->fileCsv->appendData($localFilePath, $this->errorReport, 'w');
        }
    }


    /**
     * @return bool
     */
    public function isReportFileExist()
    {
        return $this->file->fileExists($this->getReportFilePath());
    }

    /**
     * @return string
     */
    public function getReportFileName()
    {
        return $this->config->getReportFileName();
    }

    /**
     * @return bool|string
     */
    public function getReportFilePath()
    {
        try {
            $localDirPath = $this->config->getLocalDirPath();
            $reportFileName = $this->config->getReportFileName();
            return $localDirPath . $reportFileName;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
            return false;
        }
    }
}
