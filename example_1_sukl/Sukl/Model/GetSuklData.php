<?php

namespace Drmax\Sukl\Model;

use Drmax\Sukl\Api\GetSuklDataInterface;
use Drmax\Sukl\Helper\Config as ConfigHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\Io\File;
use Psr\Log\LoggerInterface;

/**
 * Class GetSuklData
 */
class GetSuklData implements GetSuklDataInterface
{
    /**
     * @var ConfigHelper
     */
    private $config;

    /**
     * @var File
     */
    private $file;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ConfigHelper $config
     * @param File $file
     * @param LoggerInterface $logger
     */
    public function __construct(
        ConfigHelper $config,
        File $file,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->file = $file;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $localFilePath = $this->downloadSuklDataFile();
        if ($localFilePath) {
            $csvData = $this->extractDataFromArchive($localFilePath);
            if ($csvData) {
                return $this->getDataFromCsv($csvData);
            }
        }
        return false;
    }

    /**
     * @return bool|string
     */
    public function downloadSuklDataFile()
    {
        $startTime = microtime(true);
        try {
            $sourceArchiveFile = $this->config->getSourceArchiveFile();
            $pathInfo = $this->file->getPathInfo($sourceArchiveFile);
            $fileName = $pathInfo['basename'];
            $localDirPath = $this->config->getLocalDirPath();
            $localFilePath = $localDirPath . $fileName;
            $this->logger->debug('Start downloading SUKL file: ' . $sourceArchiveFile);
            /* Check if file path exist, create if not */
            $this->file->checkAndCreateFolder($localDirPath);
            $result = $this->file->read($sourceArchiveFile, $localFilePath);
            if ($result) {
                $this->logger->debug(
                    'End downloading SUKL file: ' . $sourceArchiveFile,
                    ['duration' => $this->getDuration($startTime)]
                );
                return $localFilePath;
            }
            $this->logger->debug('The file was not downloaded successfully: ' . $sourceArchiveFile);
            return false;
        } catch (\Exception $e) {
            $this->logger->error(
                'The file was not downloaded successfully. Exception: ' . $e->getMessage(),
                ['exception' => $e]
            );
            return false;
        }
    }

    /**
     * @param $archiveFile
     * @return false|string
     */
    public function extractDataFromArchive($archiveFile)
    {
        $startTime = microtime(true);
        $this->logger->debug('Start extract data from archive');
        $zip = new \ZipArchive();
        $zip->open($archiveFile);
        $fileData = $zip->getFromName($this->config->getSourceFile());
        $zip->close();
        ($fileData === false)
            ? $this->logger->error('Cannot extract file from archive')
            : $this->logger->debug('End extract data from archive', ['duration' => $this->getDuration($startTime)]);
        return $fileData;
    }

    /**
     * @param $csvData
     * @return array|bool
     */
    public function getDataFromCsv($csvData)
    {
        try {
            $startTime = microtime(true);
            $this->logger->debug('Start get data from csv');
            $csv = str_getcsv($csvData, "\n");
            $returnData = [];
            $head = true;
            foreach ($csv as $row) {
                $row = str_getcsv($row, ';');
                if ($head) {
                    $this->validateHeader($row);
                    $head = false;
                } else {
                    $returnData[(int)$row[$this->config->getSuklCodeColumnId()]] = iconv(
                        'Windows-1250',
                        'UTF-8',
                        $row[$this->config->getSuklNameColumnId()] . ' ' . $row[$this->config->getSuklAdditionColumnId()]
                    );
                }
            }
            $this->logger->debug('End get data from csv', ['duration' => $this->getDuration($startTime)]);
            return $returnData;
        } catch (\Exception $e) {
            $this->logger->error('Cannot get data from csv ' . $e->getMessage(), ['exception' => $e]);
            return false;
        }
    }

    /**
     * @param array $row
     * @throws LocalizedException
     */
    private function validateHeader($row)
    {
        $csvIndex = $this->config->getCsvIndex();
        foreach ($csvIndex as $suklParameter => $columnData) {
            if ($row[$columnData['column_id']] !== $columnData['column_name']) {
                throw new LocalizedException(__(
                    'Head for %1 should be %2 not %3',
                    $suklParameter,
                    $columnData['column_name'],
                    $row[$columnData['column_id']]
                ));
            }
        }
    }

    /**
     * @param float $startTime
     * @return string
     */
    private function getDuration($startTime)
    {
        return round(microtime(true) - $startTime, 2) . 's';
    }
}
