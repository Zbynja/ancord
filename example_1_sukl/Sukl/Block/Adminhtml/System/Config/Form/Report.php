<?php

namespace Drmax\Sukl\Block\Adminhtml\System\Config\Form;

use Drmax\Sukl\Model\Report as ReportModel;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\UrlInterface;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Report
 */
class Report extends Field
{
    /**
     * @var ReportModel
     */
    private $report;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @param Context $context
     * @param ReportModel $report
     * @param UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        ReportModel $report,
        UrlInterface $urlBuilder,
        $data = []
    ) {
        $this->report = $report;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function _getElementHtml(AbstractElement $element)
    {
        if ($this->report->isReportFileExist()) {
            $url = $this->urlBuilder->getUrl('drmax_sukl/download/report');
            $element->setData(['href' => $url, 'value' => $this->report->getReportFileName()]);
        } else {
            $element->setData(['value' => 'No Report is currently available']);
        }
        return parent::_getElementHtml($element);
    }
}
