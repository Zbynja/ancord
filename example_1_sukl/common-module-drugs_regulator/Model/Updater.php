<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulator\Model;

use Drmax\DrugsRegulatorApi\Api\UpdaterInterface;
use Drmax\DrugsRegulatorApi\Api\ReportInterface as Report;
use Drmax\DrugsRegulatorApi\Api\ConfigInterface as ApiConfig;
use Drmax\DrugsRegulatorApi\Api\AttributesInterface as ApiAttributes;
use Drmax\DrugsRegulatorApi\Api\TimerInterface as Timer;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Psr\Log\LoggerInterface;

/**
 * Class Report
 */
class Updater implements UpdaterInterface
{
    /**
     * @var ApiConfig
     */
    private $config;

    /**
     * @var ApiAttributes
     */
    private $apiAttributes;

    /**
     * @var Report
     */
    private $report;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Timer
     */
    private $timer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ApiConfig $config
     * @param ApiAttributes $apiAttributes
     * @param Report $report
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param Timer $timer
     * @param LoggerInterface $logger
     */
    public function __construct(
        ApiConfig $config,
        ApiAttributes $apiAttributes,
        Report $report,
        ProductCollectionFactory $productCollectionFactory,
        ProductRepositoryInterface $productRepository,
        Timer $timer,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->apiAttributes = $apiAttributes;
        $this->report = $report;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productRepository = $productRepository;
        $this->timer = $timer;
        $this->logger = $logger;
    }

    /**
     * @param array $regulatorProductsData
     * @return void
     */
    public function updateProductAttributes(array $regulatorProductsData): void
    {
        $attributeSetIds = $this->config->getAttributeSetIds();
        $attributesToUpdate = $this->apiAttributes->getAttributesToUpdate();
        
        $productCollection = $this->productCollectionFactory->create();
        $products = $productCollection
            ->addAttributeToSelect(array_merge([ApiAttributes::DRMAX_DRUGS_REGULATOR_CODE], $attributesToUpdate))
            ->addAttributeToFilter('attribute_set_id', ['in' => $attributeSetIds])
            ->load()
        ;

        $reportItemsNotChanged = 0;
        $reportItemsChanged = 0;
        $reportItemsNotFound = 0;
        $reportErrors = 0;
        $this->timer->setStart();
        $this->logger->debug('Start - updating Drugs Regulator attributes for ' . $products->count() . ' products.');

        /** @var Product $product */
        foreach ($products as $product) {
            $productId = $product->getId();
            $drugsRegulatorId = (int)$product->getData(ApiAttributes::DRMAX_DRUGS_REGULATOR_CODE);
            $productInfo = [
                'product_id' => $productId,
                'product_sku' => $product->getSku(),
                'regulator_product_id' => $drugsRegulatorId,
            ];
            if (array_key_exists($drugsRegulatorId, $regulatorProductsData)) {
                foreach ($attributesToUpdate as $attributeCode) {
                    $existingValue = $product->getData($attributeCode);
                    $newValue = $regulatorProductsData[$drugsRegulatorId][$attributeCode];
                    if ($existingValue === $newValue) {
                        $reportItemsNotChanged++;
                    } else {
                        $product->setCustomAttribute($attributeCode, $newValue);
                        try {
                            $this->productRepository->save($product);
                            $reportItemsChanged++;
                        } catch (\Exception $e) {
                            $this->logger->error('Can\'t save Product with Id %1.', ['exception' => $e]);
                            $this->report->addErrorRow(array_merge($productInfo, ['message' => $e->getMessage()]));
                            $reportErrors++;
                        }
                    }
                }
            } else {
                $this->logger->error(__(
                    'Product %1 has Regulator ID number %2 but this was not found in the Regulator database data.',
                    $productId,
                    $drugsRegulatorId
                ));
                $this->report->addErrorRow(array_merge($productInfo, ['message' => 'Not found in Regulator database']));
                $reportItemsNotFound++;
            }
        }
        $this->logger->info(__('Drugs Regulator Updater finished: spent total %1', $this->timer->getDuration()));
        $this->logger->info(__(
            'Drugs Regulator Updater: Items changed: %1. Items not changed: %2. Items not found: %3. Errors: %4',
            $reportItemsChanged,
            $reportItemsNotChanged,
            $reportItemsNotFound,
            $reportErrors
        ));

        $this->report->saveToCsv();
    }
}
