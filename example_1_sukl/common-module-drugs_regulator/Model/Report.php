<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulator\Model;

use Drmax\DrugsRegulatorApi\Api\ReportInterface;
use Drmax\DrugsRegulatorApi\Api\ConfigInterface as Config;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\File\Csv as FileCsv;
use Magento\Framework\Filesystem\Io\File;
use Psr\Log\LoggerInterface;

/**
 * Class Report
 */
class Report implements ReportInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var File
     */
    private $file;

    /**
     * @var FileCsv
     */
    private $fileCsv;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $errorReport = [];

    /**
     * @var array
     */
    private $schemaMapping;

    /**
     * @var array
     */
    private $sortedSchemaMapping;

    /**
     * @param Config $config
     * @param File $file
     * @param FileCsv $fileCsv
     * @param LoggerInterface $logger
     * @param array $schemaMapping
     */
    public function __construct(
        Config $config,
        File $file,
        FileCsv $fileCsv,
        LoggerInterface $logger,
        array $schemaMapping = []
    ) {
        $this->config = $config;
        $this->file = $file;
        $this->fileCsv = $fileCsv;
        $this->logger = $logger;
        $this->schemaMapping = $schemaMapping;
    }


    /**
     * @inheritDoc
     */
    public function addErrorRow(array $rowData): void
    {
        if (! empty($this->schemaMapping)) {
            $filteredRowItems = [];
            $sortedSchemaMapping = $this->sortByPosition($this->schemaMapping);
            foreach ($sortedSchemaMapping as $code => $params) {
                if (isset($rowData[$code])) {
                    $filteredRowItems[] = $rowData[$code];
                }
            }
            $this->errorReport[] = $filteredRowItems;
        }
    }


    /**
     * @throws FileSystemException
     */
    public function saveToCsv(): void
    {
        $headerRow = [];
        $sortedSchemaMapping = $this->getSortedSchemaMapping();
        if (! empty($sortedSchemaMapping)) {
            foreach ($sortedSchemaMapping as $code => $params) {
                (isset($params['label']))
                    ? $headerRow[] = $params['label']
                    : $headerRow[] = 'not set'
                ;
            }
        } else {
            $headerRow[] = 'No schema defined for the output CSV file.';
        }

        array_unshift($this->errorReport, $headerRow);
        $localFilePath = $this->getFilePath();
        if ($localFilePath) {
            $this->fileCsv->appendData($localFilePath, $this->errorReport, 'w');
        }
    }


    /**
     * @return bool
     */
    public function isFileExist(): bool
    {
        return $this->file->fileExists($this->getFilePath());
    }


    /**
     * @return null|string
     */
    public function getFilePath(): ?string
    {
        try {
            $localDirPath = $this->config->getReportFileDirPath();
            $reportFileName = $this->config->getReportFileName();

            return $localDirPath . $reportFileName;

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
        }

        return null;
    }


    /**
     * @return null|string
     */
    public function getFileName(): ?string
    {
        return $this->config->getReportFileName();
    }


    /**
     * @return array
     */
    public function getSortedSchemaMapping(): array
    {
        if (! $this->sortedSchemaMapping) {
            $this->sortedSchemaMapping = [];
            if (! empty($this->schemaMapping)) {
                $this->sortedSchemaMapping = $this->sortByPosition($this->schemaMapping);
            }
        }

        return $this->sortedSchemaMapping;
    }

    /**
     * @param array $schema
     * @return array
     */
    private function sortByPosition(array $schema): array
    {
        uasort($schema, function ($item1, $item2) {
            return $item1['position'] <=> $item2['position'];
        });

        return $schema;
    }
}
