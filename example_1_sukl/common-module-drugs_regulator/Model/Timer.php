<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulator\Model;

use Drmax\DrugsRegulatorApi\Api\TimerInterface;

/**
 * Class Timer
 * @package Drmax\DrugsRegulator\Model
 */
class Timer implements TimerInterface
{
    /**
     * @var float
     */
    private $startTime;

    /**
     * @return float|null
     */
    public function getStart(): ?float
    {
        return $this->startTime;
    }

    /**
     * @return void
     */
    public function setStart(): void
    {
        $this->startTime = microtime(true);
    }

    /**
     * @return string
     */
    public function getDuration(): string
    {
        if (! $this->startTime) {
            return 'Timer start-time not set';
        }

        return round(microtime(true) - $this->startTime, 2) . 's';
    }
}
