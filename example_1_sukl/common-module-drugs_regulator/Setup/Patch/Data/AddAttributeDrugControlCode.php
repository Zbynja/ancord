<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulator\Setup\Patch\Data;

use Drmax\DrugsRegulatorApi\Api\AttributesInterface as ApiAttributes;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Zend_Validate_Exception;

/**
 * Class AddAttributeDrugControlCode
 * @package Drmax\DrugsRegulator\Setup\Patch\Data
 */
class AddAttributeDrugControlCode implements DataPatchInterface
{
    private const REGULATOR_PRODUCT_ID_SETTINGS = [
        'type' => 'varchar',
        'input' => 'text',
        'label' => 'Drugs Regulator Product Code',
        'used_in_product_listing' => true,
        'user_defined' => true,
        'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
        'visible_on_front' => true,
        'is_used_in_grid' => true,
        'is_visible_in_grid' => true,
        'is_filterable_in_grid' => true,
    ];

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $existingAttribute = $eavSetup->getAttribute(Product::ENTITY, ApiAttributes::DRMAX_DRUGS_REGULATOR_CODE);
        (isset($existingAttribute['attribute_id']))
            ? $eavSetup->updateAttribute(Product::ENTITY, ApiAttributes::DRMAX_DRUGS_REGULATOR_CODE, static::REGULATOR_PRODUCT_ID_SETTINGS)
            : $eavSetup->addAttribute(Product::ENTITY, ApiAttributes::DRMAX_DRUGS_REGULATOR_CODE, static::REGULATOR_PRODUCT_ID_SETTINGS)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases(): array
    {
        return [];
    }
}
