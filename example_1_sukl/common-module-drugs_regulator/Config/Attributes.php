<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulator\Config;

use Drmax\DrugsRegulatorApi\Api\AttributesInterface;

/**
 * Class Attributes
 * @package Drmax\DrugsRegulator\Model
 */
class Attributes implements AttributesInterface
{
    /**
     * @var array
     */
    protected $attributesToUpdate;

    /**
     * Attributes constructor.
     * @param array $attributesToUpdate
     */
    public function __construct(array $attributesToUpdate = [])
    {
        $this->attributesToUpdate = $attributesToUpdate;
    }

    /**
     * @return array
     */
    public function getAttributesToUpdate(): array
    {
        return $this->attributesToUpdate;
    }
}
