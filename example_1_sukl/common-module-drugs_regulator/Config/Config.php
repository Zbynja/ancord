<?php
declare(strict_types=1);

namespace Drmax\DrugsRegulator\Config;

use Drmax\DrugsRegulatorApi\Api\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;

/**
 * Class Config
 * @package Drmax\DrugsRegulator\Config
 */
class Config implements ConfigInterface
{
    public const CONFIG_PATH_ENABLED = 'integrations/drugs_regulator/enabled';
    public const CONFIG_PATH_REPORT_FILE_NAME = 'integrations/drugs_regulator/report_file_name';
    public const CONFIG_PATH_REPORT_FILE_DIR_PATH = 'integrations/drugs_regulator/download_dir';
    public const CONFIG_PATH_ATTRIBUTE_SETS = 'integrations/drugs_regulator/attribute_sets';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * Config constructor.
     *
     * @param DirectoryList $directoryList
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        DirectoryList $directoryList,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->directoryList = $directoryList;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return (bool)$this->scopeConfig->getValue(static::CONFIG_PATH_ENABLED);
    }

    /**
     * @return string
     */
    public function getReportFileName(): string
    {
        return $this->scopeConfig->getValue(static::CONFIG_PATH_REPORT_FILE_NAME);
    }

    /**
     * @return string
     * @throws FileSystemException
     */
    public function getReportFileDirPath(): string
    {
        $downloadDir = trim(trim($this->scopeConfig->getValue(static::CONFIG_PATH_REPORT_FILE_DIR_PATH)), '/');
        return $this->directoryList->getPath(DirectoryList::VAR_DIR) . '/' . $downloadDir . '/';
    }

    /**
     * @return array
     */
    public function getAttributeSetIds(): array
    {
        return explode(',', $this->scopeConfig->getValue(self::CONFIG_PATH_ATTRIBUTE_SETS));
    }
}
